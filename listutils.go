package main

import (
  "container/list"
  "fmt"
)

// printList iterate through names and print each.
func PrintList(names *list.List) {
  for e := names.Front(); e != nil; e = e.Next() {
    fmt.Println(e.Value)
  }
}

func CopyList(toCopy *list.List) (copy *list.List) {
  copy = list.New()
  for e := toCopy.Front(); e != nil; e = e.Next() {
    copy.PushBack(e.Value)
  }
  return copy
}