package main

import (
  "os"
  "bufio"
  "fmt"
  "container/list"
  "math/rand"
  "time"
)

// A Guest is willy waller owner, treat him with respect.
type Guest struct {
  Name string
  Id int
}

// ReadGuests read each name on the standard input and push it in a list.
// When 'ok' is read, list is return.
func ReadGuests() (guests *list.List) {
  guests = list.New()
  var id = 0
  for {
    reader := bufio.NewReader(os.Stdin)
    input, _ := reader.ReadString('\n')
    lenInput := len(input)
    if lenInput <= 1 { continue }
    input = input[:lenInput - 1]
    if input == "ok" { break }
    guests.PushBack(Guest{input, id})
    id++
  }
  return guests
}

// MatchTwoGuests match a guest with an other randomly
func MatchTwoGuests(guests *list.List) {
  guestsCopy := CopyList(guests)
  rand.Seed(time.Now().UnixNano())
  var increment bool
  var guestToRemove *list.Element
  for guest1 := guests.Front(); guest1 != nil; {
    random := rand.Intn(guestsCopy.Len())
    idx := 0
    increment = true
    for guest2 := guestsCopy.Front(); guest2 != nil; guest2 = guest2.Next() {
      if idx == random {
        if guest2.Value.(Guest).Id == guest1.Value.(Guest).Id {
          increment = false
          break
        }
        fmt.Printf("%s -> %s\n", guest1.Value.(Guest).Name, guest2.Value.(Guest).Name)
        guestToRemove = guest2
        break
      }
      idx++
    }
    if increment {
      guestsCopy.Remove(guestToRemove)
      guest1 = guest1.Next()
    }
  }
}

func main() {
  fmt.Println("Welcome to xmas rand ! (Enter \"ok\" when you are done)")
  fmt.Println("Enter some names :")
  guests := ReadGuests()
  if guests.Len() < 2 {
    fmt.Errorf("Need at least two names (just %d are entered)\n", guests.Len())
    return
  }
  MatchTwoGuests(guests)
}